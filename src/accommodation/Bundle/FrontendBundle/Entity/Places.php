<?php

namespace accommodation\Bundle\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Places
 */
class Places
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $startAt;

    /**
     * @var \DateTime
     */
    private $endAt;

    /**
     * @var string
     */
    private $city;

    /**
     * @var integer
     */
    private $numberOfBeds;

    /**
     * @var integer
     */
    private $bedCost;

    /**
     * @var integer
     */
    private $commentsCount;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Places
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     * @return Places
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime 
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     * @return Places
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime 
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Places
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set numberOfBeds
     *
     * @param integer $numberOfBeds
     * @return Places
     */
    public function setNumberOfBeds($numberOfBeds)
    {
        $this->numberOfBeds = $numberOfBeds;

        return $this;
    }

    /**
     * Get numberOfBeds
     *
     * @return integer 
     */
    public function getNumberOfBeds()
    {
        return $this->numberOfBeds;
    }

    /**
     * Set bedCost
     *
     * @param integer $bedCost
     * @return Places
     */
    public function setBedCost($bedCost)
    {
        $this->bedCost = $bedCost;

        return $this;
    }

    /**
     * Get bedCost
     *
     * @return integer 
     */
    public function getBedCost()
    {
        return $this->bedCost;
    }

    /**
     * Set commentsCount
     *
     * @param integer $commentsCount
     * @return Places
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    /**
     * Get commentsCount
     *
     * @return integer 
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * @var string
     */
    private $description;

    /**
     * Set description
     *
     * @param string $description
     * @return Places
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __toString()
    {
        return (string) $this->id;
    }

}
