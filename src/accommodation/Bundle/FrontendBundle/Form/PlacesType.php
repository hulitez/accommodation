<?php

namespace accommodation\Bundle\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlacesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('startAt')
            ->add('endAt')
            ->add('city')
            ->add('numberOfBeds')
            ->add('bedCost')
            ->add('commentsCount')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'accommodation\Bundle\FrontendBundle\Entity\Places'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'accommodation_bundle_frontendbundle_places';
    }
}
