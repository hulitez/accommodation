<?php

namespace accommodation\Bundle\FrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use accommodation\Bundle\FrontendBundle\Entity\Places;

/**
 * Places controller.
 *
 */
class ApiController extends Controller
{

    public function auth()
    {
        $em   = $this->getDoctrine()->getManager();
        $user = $em->getRepository('accommodationBundle:User')
            ->findOneBy(array('login' => $this->data['login']));

        if (count($user) > 0) {
            $hash = $user->getPass();
            if (password_verify($this->data['password'], $hash)) {
                $roles = unserialize($user->getRoles());
                if (in_array('ROLE_API_ADMIN', $roles)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Lists all Places entities.
     *
     */
    public function indexAction(Request $request)
    {
        $response    = array();
        $requestData = $request->query->get('data');
        $this->data  = json_decode($requestData, true);
        if ($this->auth()) {

            $em       = $this->getDoctrine()->getManager();
            $result   = $em->createQueryBuilder();
            $entities = $result->select('p')
                ->from('accommodationBundle:Places', 'p')
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $response = array(
                'status' => true,
                'data'   => array(
                    'places' => $entities,
                )
            );
        } else {
            $response = array(
                'status'  => false,
                'message' => 'Bad cridentials'
            );
        }
        return new JsonResponse($response);
    }

    /**
     * Creates a new Places entity.
     *
     */
    public function createAction(Request $request)
    {
        $response    = array();
        $requestData = $request->request->get('data');
        $this->data  = json_decode($requestData, true);
        if ($this->auth()) {
            $entity = new Places();
            $entity->setBedCost($this->data['place']['bedCost']);
            $entity->setCity($this->data['place']['city']);
            $entity->setDescription($this->data['place']['description']);
            $entity->setEndAt(new \DateTime($this->data['place']['endAt']));
            $entity->setName($this->data['place']['name']);
            $entity->setNumberOfBeds($this->data['place']['numberOfBeds']);
            $entity->setStartAt(new \DateTime($this->data['place']['startAt']));

            try {
                $em       = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $response = array(
                    'status'  => true,
                    'message' => 'created',
                    'id'      => $entity->getId(),
                );
            } catch (Exception $ex) {
                $response = array(
                    'status'  => false,
                    'message' => 'not created'
                );
            }
        } else {
            $response = array(
                'status'  => false,
                'message' => 'Bad cridentials'
            );
        }
        return new JsonResponse($response);
    }

    /**
     * Finds and displays a Places entity.
     *
     */
    public function showAction($id)
    {
        $response    = array();
        $requestData = $this->getRequest()->query->get('data');
        $this->data  = json_decode($requestData, true);
        if ($this->auth()) {

            $em = $this->getDoctrine()->getManager();

            $result = $em->createQueryBuilder();
            $entity = $result->select('p')
                ->from('accommodationBundle:Places', 'p')
                ->where('p.id= :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            if (!$entity) {
                $response = array(
                    'status'  => false,
                    'message' => 'Unable to find Places entity.'
                );
                return new JsonResponse($response);
            }

            $response = array(
                'status' => true,
                'data'   => array(
                    'place' => $entity,
                )
            );
        } else {
            $response = array(
                'status'  => false,
                'message' => 'Bad cridentials'
            );
        }
        return new JsonResponse($response);
    }

    /**
     * Edits an existing Places entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $requestData = $request->request->get('data');
        $this->data  = json_decode($requestData, true);
        if ($this->auth()) {

            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('accommodationBundle:Places')->find($id);

            if (!$entity) {
                $response = array(
                    'status'  => false,
                    'message' => 'Unable to find Places entity.'
                );
                return new JsonResponse($response);
            }

            $entity->setBedCost($this->data['place']['bedCost']);
            $entity->setCity($this->data['place']['city']);
            $entity->setDescription($this->data['place']['description']);
            $entity->setEndAt(new \DateTime($this->data['place']['endAt']));
            $entity->setName($this->data['place']['name']);
            $entity->setNumberOfBeds($this->data['place']['numberOfBeds']);
            $entity->setStartAt(new \DateTime($this->data['place']['startAt']));

            try {
                $em->flush();
                $response = array(
                    'status'  => true,
                    'message' => 'updated',
                    'id'      => $entity->getId(),
                );
            } catch (Exception $ex) {
                $response = array(
                    'status'  => false,
                    'message' => 'not updated'
                );
            }
        } else {
            $response = array(
                'status'  => false,
                'message' => 'Bad cridentials'
            );
        }
        return new JsonResponse($response);
    }

    /**
     * Deletes a Places entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $requestData = $request->request->get('data');
        $this->data  = json_decode($requestData, true);
        if ($this->auth()) {

            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('accommodationBundle:Places')->find($id);

            if (!$entity) {
                $response = array(
                    'status'  => false,
                    'message' => 'Unable to find Places entity.'
                );
                return new JsonResponse($response);
            }

            $comments = $em->getRepository('accommodationBundle:Comments')
                ->findBy(array('place' => $entity));

            try {
                foreach ($comments as $comment) {
                    $em->remove($comment);
                    $em->flush();
                }

                $em->remove($entity);
                $em->flush();

                $response = array(
                    'status'  => true,
                    'message' => 'deleted',
                    'id'      => $id,
                );
            } catch (Exception $ex) {
                $response = array(
                    'status'  => false,
                    'message' => 'not deleted'
                );
            }
        } else {
            $response = array(
                'status'  => false,
                'message' => 'Bad cridentials'
            );
        }
        return new JsonResponse($response);
    }

    public function checkUserRole()
    {
        $user = $this->getUser();
        if (!is_null($user)) {
            $userRoles = $user->getRoles();
            if (in_array('ROLE_ADMIN', $userRoles)) {
                $this->isAdmin = true;
            }
        }
        return $this;
    }

}
