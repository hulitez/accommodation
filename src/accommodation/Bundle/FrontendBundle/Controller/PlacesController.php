<?php

namespace accommodation\Bundle\FrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use accommodation\Bundle\FrontendBundle\Entity\Places;
use accommodation\Bundle\FrontendBundle\Controller\CommentsController;
use accommodation\Bundle\FrontendBundle\Form\PlacesType;
use accommodation\Bundle\FrontendBundle\Entity\Comments;
use accommodation\Bundle\FrontendBundle\Form\CommentsType;

/**
 * Places controller.
 *
 */
class PlacesController extends Controller
{

    private $isAdmin = false;
    private $isUser  = false;

    /**
     * Lists all Places entities.
     *
     */
    public function indexAction()
    {
        $this->checkUserRole();
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('accommodationBundle:Places')->findAll();

        return $this->render('accommodationBundle:Places:index.html.twig', array(
                'entities' => $entities,
        ));
    }

    /**
     * Creates a new Places entity.
     *
     */
    public function createAction(Request $request)
    {
        $this->checkUserRole();
        if (!$this->isAdmin) {
            $referer = $this->getRequest()
                ->headers->get('referer');
            if ($referer) {
                return $this->redirect($referer);
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        $entity = new Places();
        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('places_show', array('id' => $entity->getId())));
        }

        return $this->render('accommodationBundle:Places:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Places entity.
     *
     * @param Places $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Places $entity)
    {
        $form = $this->createForm(new PlacesType(), $entity, array(
            'action' => $this->generateUrl('places_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Places entity.
     *
     */
    public function newAction()
    {
        $this->checkUserRole();
        if (!$this->isAdmin) {
            $referer = $this->getRequest()
                ->headers->get('referer');
            if ($referer) {
                return $this->redirect($referer);
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        $entity = new Places();
        $form   = $this->createCreateForm($entity);

        return $this->render('accommodationBundle:Places:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Places entity.
     *
     */
    public function showAction($id)
    {
        $this->checkUserRole();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('accommodationBundle:Places')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Places entity.');
        }

        $deleteForm   = $this->createDeleteForm($id);
        $commentsForm = $this->newPlaceCommentAction($entity);

        $comments = $em->getRepository('accommodationBundle:Comments')
            ->findBy(array('place' => $entity));

        return $this->render('accommodationBundle:Places:show.html.twig', array(
                'entity'       => $entity,
                'delete_form'  => $deleteForm->createView(),
                'commentsForm' => $commentsForm->createView(),
                'comments'     => $comments,
        ));
    }

    /**
     * Displays a form to edit an existing Places entity.
     *
     */
    public function editAction($id)
    {
        $this->checkUserRole();
        if (!$this->isAdmin) {
            $referer = $this->getRequest()
                ->headers->get('referer');
            if ($referer) {
                return $this->redirect($referer);
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('accommodationBundle:Places')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Places entity.');
        }

        $editForm   = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('accommodationBundle:Places:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Places entity.
     *
     * @param Places $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Places $entity)
    {
        $form = $this->createForm(new PlacesType(), $entity, array(
            'action' => $this->generateUrl('places_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Places entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->checkUserRole();
        if (!$this->isAdmin) {
            $referer = $this->getRequest()
                ->headers->get('referer');
            if ($referer) {
                return $this->redirect($referer);
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('accommodationBundle:Places')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Places entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('places_edit', array('id' => $id)));
        }

        return $this->render('accommodationBundle:Places:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Places entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->checkUserRole();
        if (!$this->isAdmin) {
            $referer = $this->getRequest()
                ->headers->get('referer');
            if ($referer) {
                return $this->redirect($referer);
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        }
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('accommodationBundle:Places')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Places entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('places'));
    }

    /**
     * Creates a form to delete a Places entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('places_delete', array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'submit', array('label' => 'Delete'))
                ->getForm()
        ;
    }

    public function newPlaceCommentAction($place)
    {
        $user     = $this->getUser();
        $userName = $user->getUsername();
        $entity   = new Comments();
        $entity->setPlace($place);
        $entity->setNick($userName);
        $form     = $this->createPlaceCommentCreateForm($entity);

        return $form;
    }

    /**
     * Creates a form to create a Comments entity.
     *
     * @param Comments $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPlaceCommentCreateForm(Comments $entity)
    {
        $form = $this->createForm(new CommentsType(), $entity, array(
            'action' => $this->generateUrl('comments_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    public function checkUserRole()
    {
        $user = $this->getUser();
        if (!is_null($user)) {
            $userRoles = $user->getRoles();
            if (in_array('ROLE_ADMIN', $userRoles)) {
                $this->isAdmin = true;
            }
        }
        return $this;
    }

}
