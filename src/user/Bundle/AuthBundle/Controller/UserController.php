<?php

namespace user\Bundle\AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use user\Bundle\AuthBundle\Entity\User;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                        'userAuthBundle:User:login.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error'         => $error,
                        )
        );
    }

    public function error403Action()
    {
        return $this->render('userAuthBundle:User:error403.html.twig', array(
                    'error' => 'Niestety nie masz wystarczających uprawnień by zobaczyć zawartość strony do której próbujesz uzyskać dostęp'
        ));
    }

    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('userAuthBundle:User')->findAll();

        return $this->render('userAuthBundle:User:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('userAuthBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return $this->render('userAuthBundle:User:show.html.twig', array(
                    'entity' => $entity,
        ));
    }

}
