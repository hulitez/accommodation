<?php

/**
 * Description of testApi
 *
 * @author zjedrzejczak
 */
class testApi
{

    const URL = 'http://accommodation.loc/api';

    public static function test($testType)
    {
        $data['create'] = array('data'   => array(
                'data' => json_encode(array(
                    'password' => 'test123',
                    'login'    => 'apiadmin',
                    'place'    => array(
                        'bedCost'      => '200',
                        'city'         => 'Szczecin',
                        'description'  => 'Gruba impreza Sylwestrowa dla znajomych',
                        'endAt'        => '2015-01-01',
                        'name'         => 'Sylwester',
                        'numberOfBeds' => '50',
                        'startAt'      => '2014-12-31',
                    ),
                )),
            ),
            'query'  => '/place',
            'method' => 'POST',
        );

        $data['show'] = array('data'   => array(
                'data' => json_encode(array(
                    'password' => 'test123',
                    'login'    => 'apiadmin',
                )),
            ),
            'query'  => '/place/1',
            'method' => 'GET',
        );

        $data['update'] = array('data'   => array(
                'data' => json_encode(array(
                    'password' => 'test123',
                    'login'    => 'apiadmin',
                    'place'    => array(
                        'bedCost'      => '200',
                        'city'         => 'Szczecin',
                        'description'  => 'Gruba impreza Sylwestrowa dla znajomych (UPDATE)',
                        'endAt'        => '2015-01-01',
                        'name'         => 'Sylwester',
                        'numberOfBeds' => '50',
                        'startAt'      => '2014-12-31',
                    ),
                )),
            ),
            'query'  => '/place/1',
            'method' => 'PUT',
        );

        $data['list'] = array('data'   => array(
                'data' => json_encode(array(
                    'password' => 'test123',
                    'login'    => 'apiadmin',
                )),
            ),
            'query'  => '/place',
            'method' => 'GET',
        );

        $data['delete'] = array('data'   => array(
                'data' => json_encode(array(
                    'password' => 'test123',
                    'login'    => 'apiadmin',
                )),
            ),
            'query'  => '/place/2',
            'method' => 'DELETE',
        );

        $headers = array(
            'Accept-Language' => "pl",
            'Accept'          => "application/json",
            'Content-type'    => "application/json; charset=utf-8",
            'ignore_errors'   => true,
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $data[$testType]['method']);
        if ($data[$testType]['method'] == 'GET') {
            curl_setopt($curl, CURLOPT_URL, self::URL . $data[$testType]['query']
                . '?' . http_build_query($data[$testType]['data']));
            curl_setopt($curl, CURLOPT_HTTPGET, TRUE);
        } else {
            curl_setopt($curl, CURLOPT_URL, self::URL . $data[$testType]['query']);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data[$testType]['data']));
            curl_setopt($curl, CURLOPT_POST, count($data));
        }

        $result = curl_exec($curl);
        if ($result === false) {
            error_log('Curl error: ' . curl_error($curl));
        }
        return $result;
    }

}

echo testApi::test('create');
echo testApi::test('show');
echo testApi::test('update');
echo testApi::test('list');
echo testApi::test('delete');
